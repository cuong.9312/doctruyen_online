package com.activites;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import Model.Book;
import Model.Item;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class OnlineOther extends Activity {
	ProgressDialog mProgressDialog;
	TextView tViewOnline;
	ActionBar actionBar;
	ListView listView;
	ArrayList<Book> arr;
	ArrayAdapter<Book> adapter;
	static ArrayList<Book> arrKhac = new ArrayList<Book>();
	static int numberOfPage=1;
	Item item;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_online_other);
		getControls();
		inits();
		addEvents();
	}
	public void getControls() {
		listView = (ListView) findViewById(R.id.listOnlineOther);
	}

	public void inits() {
		try {
			setFontAndActionBar();
			this.setTitle("Truyện khác");

				arr = arrKhac;
			if (arr.size() == 0) {
				new Title().execute();
			}
			adapter = new ArrayAdapter<Book>(this,
					android.R.layout.simple_list_item_1, arr);
			listView.setAdapter(adapter);

		} catch (Exception ex) {
			Toast.makeText(getBaseContext(), ex.toString(), Toast.LENGTH_SHORT)
					.show();
		}

	}

	public void addEvents() {
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(position==arr.size()-1){
					new Title().execute();
					adapter.notifyDataSetChanged();
				}
				else{
					Book book = arr.get(position);
					Intent intent = new Intent(getBaseContext(),
							PageReadOnlineOther.class);
					intent.putExtra("book", book);
					startActivityForResult(intent, 1);
				}
				
			}
		});

	}
	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		 actionBar.setIcon(R.drawable.m4);
		actionBar.setDisplayHomeAsUpEnabled(true);
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}
	public void getBook() {
	try {
		if(numberOfPage>1&&numberOfPage<=9){
			arr.remove(arr.size()-1);
			Document doc;
			doc = Jsoup
					.connect("http://doctruyen360.net/truyen-co-tich/page/"+numberOfPage)
					.userAgent(
							"Mozilla / 5.0 (Windows NT 6.0) AppleWebKit / 536.5 (KHTML, like Gecko) Chrome / 19.0.1084.46 Safari / 536.5 ")
					.timeout(100000).ignoreHttpErrors(true).execute().parse();

			// ===========================================
			Elements div = doc.select("div[id=tindaidien]");
			for(int i=0;i<div.size();i++){
				Element link=div.get(i).getElementsByTag("a").first();
				String text = link.text();
				String url=link.attr("href");
				Book bookOnline = new Book(url, text, "", "");
				arr.add(bookOnline);
			}
			arr.add(new Book("LoadMore","Tải thêm...","",""));
			numberOfPage++;
		}
		if(numberOfPage==1){
			Document doc;
			doc = Jsoup
					.connect("http://doctruyen360.net/truyen-co-tich")
					.userAgent(
							"Mozilla / 5.0 (Windows NT 6.0) AppleWebKit / 536.5 (KHTML, like Gecko) Chrome / 19.0.1084.46 Safari / 536.5 ")
					.timeout(100000).ignoreHttpErrors(true).execute().parse();

			// ===========================================
			Elements div = doc.select("div[id=tindaidien]");
			for(int i=0;i<div.size();i++){
				Element link=div.get(i).getElementsByTag("a").first();
				String text = link.text();
				String url=link.attr("href");
				Book bookOnline = new Book(url, text, "", "");
				arr.add(bookOnline);
			}
			arr.add(new Book("LoadMore","Tải thêm...","",""));
			numberOfPage++;
		}


	} catch (Exception e) {
		e.printStackTrace();
	}
}
	private class Title extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(OnlineOther.this);
			// mProgressDialog.setTitle("Getting data...");
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
					getBook();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Set title into TextView
			adapter.notifyDataSetChanged();
			mProgressDialog.dismiss();
			if (arr.size() == 0) {
				Toast.makeText(getBaseContext(),
						"Lỗi kết nối, vào menu để tải lại.", Toast.LENGTH_LONG)
						.show();
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.online_other, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}
	private boolean MenuChoice(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.reload:
			for (int i = arr.size() - 1; i >= 0; i--) {
				arr.remove(i);
			}
			numberOfPage=1;
			finish();
			startActivity(getIntent());
			return true;
		}
		return false;
	}
}
