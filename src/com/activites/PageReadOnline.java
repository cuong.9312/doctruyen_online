package com.activites;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import Model.Book;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class PageReadOnline extends Activity {

	TextView tView;
	Float sizeInit;
	ProgressDialog mProgressDialog;
	String url;
	Book bookOnline;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_page_read_online);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		tView = (TextView) findViewById(R.id.tView);
	}

	public void inits() {
		setFontAndActionBar();
		Intent intent = getIntent();
		bookOnline = (Book) intent.getSerializableExtra("book");
		this.setTitle(bookOnline.getTitle());
		url = bookOnline.getUrl();
		try {
			if (Online.mapBook.containsKey(bookOnline.getTitle())) {
				bookOnline = Online.mapBook.get(bookOnline.getTitle());
				tView.setText(bookOnline.getContent());
			} else {
				new Title().execute();
			}

		} catch (Exception ex) {
			Toast.makeText(getBaseContext(), ex.toString(), Toast.LENGTH_LONG)
					.show();
		}

		sizeInit = (float) 20;
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		 actionBar.setIcon(R.drawable.m6);
		actionBar.setDisplayHomeAsUpEnabled(true);
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
		tView.setTypeface(custom_font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.page_read_online, menu);
		return true;
	}

	public void addEvents() {
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	private boolean MenuChoice(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.menu_zoomin:
			zoomIn();
			return true;
		case R.id.menu_zoomout:
			zoomOut();
			return true;
		case R.id.menu_save:
			save();
			return true;
		case R.id.dark:
			setDark();
			return true;
		case R.id.light:
			setLight();
			return true;
		case R.id.menu_setting:
			Toast.makeText(this, "You clicked on Item setting",
					Toast.LENGTH_LONG).show();
			return true;
		case R.id.reload:
			Online.mapBook.remove(bookOnline.getTitle());
			finish();
			startActivity(getIntent());
			return true;
		}
		return false;
	}
	public void setDark() {
		tView.setBackgroundColor(Color.parseColor("#000000"));
		tView.setTextColor(Color.parseColor("#FFFFFF"));
	}
	public void setLight() {
		tView.setBackgroundColor(Color.parseColor("#FFFFFF"));
		tView.setTextColor(Color.parseColor("#000000"));
	}

	public void save() {
		try {

			// ---SD Card Storage---
			File sdCard = Environment.getExternalStorageDirectory();
			File directory = new File(sdCard.getAbsolutePath() + "/doctruyen");
			directory.mkdirs();
			File file = new File(directory, bookOnline.getTitle() + ".txt");
			FileOutputStream fOut = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(fOut);
			osw.write(tView.getText().toString());
			osw.flush();
			osw.close();
			Toast.makeText(this, "lưu thành công", Toast.LENGTH_SHORT).show();
		} catch (Exception ex) {
			Toast.makeText(getBaseContext(), ex.toString(), Toast.LENGTH_LONG)
					.show();
		}
	}

	public void zoomIn() {
		if (sizeInit < 100) {
			sizeInit += 2;
		}
		tView.setTextSize(sizeInit);
	}

	public void zoomOut() {
		if (sizeInit > 4) {
			sizeInit -= 2;
		}
		tView.setTextSize(sizeInit);
	}

	private class Title extends AsyncTask<Void, Void, Void> {
		String content;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(PageReadOnline.this);
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				Document doc;
				doc = Jsoup
						.connect(url)
						.userAgent(
								"Mozilla / 5.0 (Windows NT 6.0) AppleWebKit / 536.5 (KHTML, like Gecko) Chrome / 19.0.1084.46 Safari / 536.5 ")
						.timeout(100000).ignoreHttpErrors(true).execute()
						.parse();
				Element div = doc.select("span[class=content]").first();
				content = div.text();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Set title into TextView
			tView.setText(content);
			bookOnline.setContent(content);
			Online.mapBook.put(bookOnline.getTitle(), bookOnline);
			mProgressDialog.dismiss();
			if (tView.getText().toString() == ""
					|| tView.getText().toString().equals("")
					|| tView.getText().toString() == null) {
				tView.setText("Lỗi kết nối, vào menu để thử tải lại!");
			}

		}
	}
}
