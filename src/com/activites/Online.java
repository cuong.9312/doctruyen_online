package com.activites;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import Model.Book;
import Model.Item;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Online extends Activity {
	ProgressDialog mProgressDialog;
	TextView tViewOnline;
	ActionBar actionBar;
	ListView listView;
	ArrayList<Book> arr;
	ArrayAdapter<Book> adapter;
	static ArrayList<Book> arrCoTich = new ArrayList<Book>();
	static ArrayList<Book> arrTruyenThuyet = new ArrayList<Book>();
	static ArrayList<Book> arrAndersen = new ArrayList<Book>();
	static ArrayList<Book> arrGrimm = new ArrayList<Book>();
	static ArrayList<Book> arrTheGioi = new ArrayList<Book>();
	static ArrayList<Book> arrThanThoai = new ArrayList<Book>();
	static ArrayList<Book> arrTruyenTranh = new ArrayList<Book>();

	static HashMap<String, Book> mapBook = new HashMap<String, Book>();
	Item item;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_online);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		listView = (ListView) findViewById(R.id.listOnline);
	}

	public void inits() {
		try {
			setFontAndActionBar();
			item = (Item) getIntent().getSerializableExtra("item");
			this.setTitle(item.getCategory());
			if (item.getId().equals("coTich")) {
				arr = arrCoTich;
			}
			if (item.getId().equals("truyenThuyet")) {
				arr = arrTruyenThuyet;
			}
			if (item.getId().equals("andersen")) {
				arr = arrAndersen;
			}
			if (item.getId().equals("grimm")) {
				arr = arrGrimm;
			}
			if (item.getId().equals("theGioi")) {
				arr = arrTheGioi;
			}
			if (item.getId().equals("thanThoai")) {
				arr = arrThanThoai;
			}
			if (arr.size() == 0) {
				new Title().execute();
			}
			adapter = new ArrayAdapter<Book>(this,
					android.R.layout.simple_list_item_1, arr);
			listView.setAdapter(adapter);

		} catch (Exception ex) {
			Toast.makeText(getBaseContext(), ex.toString(), Toast.LENGTH_SHORT)
					.show();
		}

	}

	public void addEvents() {
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Book book = arr.get(position);
				Intent intent = new Intent(getBaseContext(),
						PageReadOnline.class);
				intent.putExtra("book", book);
				startActivityForResult(intent, 1);
			}
		});

	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		 actionBar.setIcon(R.drawable.m3);
		actionBar.setDisplayHomeAsUpEnabled(true);
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	public void getBook(String page) {
		try {
			Document doc;
			doc = Jsoup
					.connect(page)
					// "http://vanhoc.xitrum.net/truyencotich/vietnam")
					.userAgent(
							"Mozilla / 5.0 (Windows NT 6.0) AppleWebKit / 536.5 (KHTML, like Gecko) Chrome / 19.0.1084.46 Safari / 536.5 ")
					.timeout(100000).ignoreHttpErrors(true).execute().parse();

			// ===========================================
			Element div = doc.select("table[class=maintable]").first();
			Elements links = div.getElementsByTag("a").not(".nav");
			for (int i = 0; i < links.size() - 6; i++) {
				Element link = links.get(i);
				String text = link.text();
				String url = "http://vanhoc.xitrum.net" + link.attr("href");
				// title += text + "\n" + url + "\n================\n";
				Book bookOnline = new Book(url, text, "", "");
				arr.add(bookOnline);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
//	public void getBookOther() {
//		try {
//			Document doc;
//			doc = Jsoup
//					.connect("http://doctruyen360.net/truyen-co-tich")
//					.userAgent(
//							"Mozilla / 5.0 (Windows NT 6.0) AppleWebKit / 536.5 (KHTML, like Gecko) Chrome / 19.0.1084.46 Safari / 536.5 ")
//					.timeout(100000).ignoreHttpErrors(true).execute().parse();
//
//			// ===========================================
//			Elements div = doc.select("div[id=tindaidien]");
//			for(int i=0;i<div.size();i++){
//				Element link=div.get(i).getElementsByTag("a").first();
//				String text = link.text()+link.attr("href");
//				String url=link.attr("href");
//			}
//			Elements links = div.getElementsByTag("a").not(".nav");
//			for (int i = 0; i < links.size() - 6; i++) {
//				Element link = links.get(i);
//				String text = link.text();
//				String url = "http://vanhoc.xitrum.net" + link.attr("href");
//				// title += text + "\n" + url + "\n================\n";
//				Book bookOnline = new Book(url, text, "", "");
//				arr.add(bookOnline);
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}

	private class Title extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(Online.this);
			// mProgressDialog.setTitle("Getting data...");
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
					getBook(item.getUrl());
//				Document doc;
//				doc = Jsoup
//						.connect(
//						// "http://192.168.56.1:8080/web_truyen/Grimm.html")
//						// "http://www.grimmstories.com/vi/grimm_truyen/list")
//								item.getUrl())
//						.userAgent(
//								"Mozilla / 5.0 (Windows NT 6.0) AppleWebKit / 536.5 (KHTML, like Gecko) Chrome / 19.0.1084.46 Safari / 536.5 ")
//						.timeout(100000).ignoreHttpErrors(true).execute()
//						.parse();
//
//				// ===========================================
//				Element div = doc.select("table[class=maintable]").first();
//				Elements links = div.getElementsByTag("a").not(".nav");
//				for (int i = 0; i < links.size() - 6; i++) {
//					Element link = links.get(i);
//					String url = "http://vanhoc.xitrum.net" + link.attr("href");
//					String text = link.text();
//					// title += text + "\n" + url + "\n================\n";
//					Book bookOnline = new Book(url, text, "", "");
//					arr.add(bookOnline);
//				}
//
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Set title into TextView
			adapter.notifyDataSetChanged();
			mProgressDialog.dismiss();
			if (arr.size() == 0) {
				Toast.makeText(getBaseContext(),
						"Lỗi kết nối, vào menu để tải lại.", Toast.LENGTH_LONG)
						.show();
			}

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.online, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	private boolean MenuChoice(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.reload:
			for (int i = arr.size() - 1; i >= 0; i--) {
				arr.remove(i);
			}
			finish();
			startActivity(getIntent());
			return true;
		}
		return false;
	}
}
