package com.activites;

import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import Model.Book;
import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class SearchResult extends Activity {
	String query;
	ProgressDialog mProgressDialog;
	ListView listSearch;
	ArrayList<Book> arr;
	ArrayAdapter<Book> adapter;
	ArrayList<Book> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_search_result);
		setFontAndActionBar();
		getControls();
		inits();
		 addEvents();
	}

	public void getControls() {
		listSearch = (ListView) findViewById(R.id.listSearch);
	}

	public void inits() {
		handleIntent(getIntent());

		Toast.makeText(getBaseContext(), query, Toast.LENGTH_LONG).show();
		arr = new ArrayList<Book>();
		list = new ArrayList<Book>();
		if (arr.size() == 0) {
			new Title().execute();
		}
		adapter = new ArrayAdapter<Book>(this,
				android.R.layout.simple_list_item_1, arr);
		listSearch.setAdapter(adapter);
	}

	public void addEvents() {
		listSearch.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Book book = arr.get(position);
				Intent intent = new Intent(getBaseContext(),
						PageReadOnline.class);
				intent.putExtra("book", book);
				startActivityForResult(intent, 1);
			}
		});
	}

	private class Title extends AsyncTask<Void, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			mProgressDialog = new ProgressDialog(SearchResult.this);
			// mProgressDialog.setTitle("Getting data...");
			mProgressDialog.setMessage("Đang tìm kiếm...");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			getBook("http://vanhoc.xitrum.net/truyencotich/vietnam");
//			getBook("http://vanhoc.xitrum.net/truyencotich/truyenthuyet");
//			getBook("http://vanhoc.xitrum.net/truyencotich/andersen");
//			getBook("http://vanhoc.xitrum.net/truyencotich/grim");
//			getBook("http://vanhoc.xitrum.net/truyencotich/thegioi");
//			getBook("http://vanhoc.xitrum.net/truyencotich/thanthoai");
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// Set title into TextView
			adapter.notifyDataSetChanged();
			mProgressDialog.dismiss();
			if (arr.size() == 0) {
				Toast.makeText(getBaseContext(),
						"Không tìm thấy kết quả nào phù hợp!.", Toast.LENGTH_LONG)
						.show();
			}

		}
	}

	public void getBook(String page) {
		try {
			Document doc;
			doc = Jsoup
					.connect(page)
					// "http://vanhoc.xitrum.net/truyencotich/vietnam")
					.userAgent(
							"Mozilla / 5.0 (Windows NT 6.0) AppleWebKit / 536.5 (KHTML, like Gecko) Chrome / 19.0.1084.46 Safari / 536.5 ")
					.timeout(100000).ignoreHttpErrors(true).execute().parse();

			// ===========================================
			Element div = doc.select("table[class=maintable]").first();
			Elements links = div.getElementsByTag("a").not(".nav");
			for (int i = 0; i < links.size() - 6; i++) {
				Element link = links.get(i);
				String text = link.text();
				if (text.toUpperCase().contains(query.toUpperCase())) {
					String url = "http://vanhoc.xitrum.net" + link.attr("href");
					// title += text + "\n" + url + "\n================\n";
					Book bookOnline = new Book(url, text, "", "");
					arr.add(bookOnline);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		 actionBar.setIcon(R.drawable.m8);
		actionBar.setDisplayHomeAsUpEnabled(true);
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		setIntent(intent);
		handleIntent(intent);
	}

	/**
	 * Handling intent data
	 */
	private void handleIntent(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			query = intent.getStringExtra(SearchManager.QUERY);
		}

	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_result, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.reload:
			finish();
			startActivity(getIntent());
			return true;
		}
		return false;
	}

}
