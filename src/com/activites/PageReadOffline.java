package com.activites;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import Model.Book;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class PageReadOffline extends Activity {

	TextView tView;
	Float sizeInit;
	Book book;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_page_read);
		getControls();
		inits();
		// addEvents();
	}

	public void getControls() {
		tView = (TextView) findViewById(R.id.tView);
	}

	public void inits() {
		setFontAndActionBar();
		Intent intent = getIntent();
		book = (Book) intent.getSerializableExtra("info");
		this.setTitle(book.getTitle());
		readFile();

		sizeInit = (float) 20;
	}

	public void readFile() {
		String content = "";
		try {
			// ---SD Card Storage---
			File sdCard = Environment.getExternalStorageDirectory();
			File directory = new File(sdCard.getAbsolutePath() + "/doctruyen");
			File file = new File(directory, book.getUrl());
			FileInputStream in = new FileInputStream(file);
			BufferedReader bufreader = new BufferedReader(
					new InputStreamReader(in));
			String data = "";
			if (in != null) {
				while ((data = bufreader.readLine()) != null) {
					content += data + "\n";
				}
				in.close();
			}
		} catch (Exception ex) {

		}

		tView.setText(content);
	}

	public void deleteFile() {
		try {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(
					PageReadOffline.this);
			alertDialog.setMessage("Bạn chắc chắn muốn xóa truyện?");
			alertDialog.setIcon(R.drawable.delete);
			alertDialog.setPositiveButton("YES",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// ---SD Card Storage---
							File sdCard = Environment
									.getExternalStorageDirectory();
							File directory = new File(sdCard.getAbsolutePath()
									+ "/doctruyen");
							File file = new File(directory, book.getUrl());
							file.delete();
							setResult(Offline.deleteBack);
							finish();

						}
					});
			alertDialog.setNegativeButton("NO",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			alertDialog.show();
		} catch (Exception ex) {

		}

	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		 actionBar.setIcon(R.drawable.m5);
		actionBar.setDisplayHomeAsUpEnabled(true);
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
		tView.setTypeface(custom_font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.page_read, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		// TODO Auto-generated method stub
//		super.onActivityResult(requestCode, resultCode, data);
//		if (resultCode == 2) {
//			String size = data.getStringExtra("size");
//			// Toast.makeText(getBaseContext(), size, Toast.LENGTH_LONG).show();
//			tView.setTextSize(Float.parseFloat(size));
//		}
//	}

	private boolean MenuChoice(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case R.id.menu_zoomin:
			zoomIn();
			return true;
		case R.id.menu_zoomout:
			zoomOut();
			return true;
		case R.id.menu_delete:
			deleteFile();
			return true;
		case R.id.dark:
			setDark();
			return true;
		case R.id.light:
			setLight();
			return true;
		case R.id.menu_setting:
			 Toast.makeText(this, "You clicked on Item 5", Toast.LENGTH_LONG)
			 .show();
			return true;
		}
		return false;
	}
	public void setDark() {
		tView.setBackgroundColor(Color.parseColor("#000000"));
		tView.setTextColor(Color.parseColor("#FFFFFF"));
	}
	public void setLight() {
		tView.setBackgroundColor(Color.parseColor("#FFFFFF"));
		tView.setTextColor(Color.parseColor("#000000"));
	}

	public void zoomIn() {
		if (sizeInit < 100) {
			sizeInit += 2;
		}
		tView.setTextSize(sizeInit);
	}

	public void zoomOut() {
		if (sizeInit > 4) {
			sizeInit -= 2;
		}
		tView.setTextSize(sizeInit);
	}
}
