package com.activites;

import java.util.ArrayList;

import Model.Item;
import MyAdappter.MyArrayAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class MainActivity extends Activity {
	ListView listViewItem;
	ArrayList<Item> arr = new ArrayList<Item>();
	MyArrayAdapter adapter = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		listViewItem = (ListView) findViewById(R.id.listViewItem);
	}

	public void inits() {
		setFontAndActionBar();

		arr = new ArrayList<Item>();
		arr.add(new Item("coTich", "Truyện cổ Việt Nam",
				"http://vanhoc.xitrum.net/truyencotich/vietnam"));
		arr.add(new Item("truyenThuyet", "Truyền thuyết và giai thoại",
				"http://vanhoc.xitrum.net/truyencotich/truyenthuyet/"));
		arr.add(new Item("andersen", "Truyện cổ Andersen",
				"http://vanhoc.xitrum.net/truyencotich/andersen/"));
		arr.add(new Item("grimm", "Truyện cổ Grimm",
				"http://vanhoc.xitrum.net/truyencotich/grim/"));
		arr.add(new Item("theGioi", "Truyện cổ thế giới",
				"http://vanhoc.xitrum.net/truyencotich/thegioi/"));
		arr.add(new Item("thanThoai", "Thần thoại Hy Lạp & La Mã",
				"http://vanhoc.xitrum.net/truyencotich/thanthoai/"));
		arr.add(new Item("khac", "Truyện khác",
				"http://doctruyen360.net/truyen-co-tich"));
		arr.add(new Item("daLuu", "Truyện đã lưu"));
		adapter = new MyArrayAdapter(this, R.layout.custom_listview, arr);
		listViewItem.setAdapter(adapter);
	}

	public void addEvents() {
		listViewItem.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Item item = arr.get(position);
				if(item.getId().equalsIgnoreCase("khac")){
						Intent intent = new Intent(getBaseContext(), OnlineOther.class);
						startActivity(intent);
					return;
				}
				if (item.getId().equalsIgnoreCase("daLuu")) {
					Intent intent = new Intent(getBaseContext(), Offline.class);
					startActivity(intent);
					return;
				}
				nextIntent(arr.get(position));

			}
		});
	}

	public void nextIntent(Item item) {
		Intent intent = new Intent(getBaseContext(), Online.class);
		intent.putExtra("item", item);
		startActivity(intent);
	}

	public void setFontAndActionBar() {
		 ActionBar actionBar = getActionBar();
		 actionBar.setIcon(R.drawable.m1);
		// actionBar.setDisplayHomeAsUpEnabled(true);
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);

		// Associate searchable configuration with the SearchView
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) menu.findItem(R.id.action_search)
				.getActionView();
		searchView.setSearchableInfo(searchManager
				.getSearchableInfo(getComponentName()));

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		// case android.R.id.home:
		// Toast.makeText(this, "You clicked on the Application icon",
		// Toast.LENGTH_LONG).show();
		// return true;
		case R.id.action_search:
			return true;
		case R.id.menu_about:
			Intent intent = new Intent(getBaseContext(), About.class);
			startActivity(intent);
			return true;
		case R.id.menu_donate:
			sendEmail();
			return true;
		}
		return false;
	}

	private void sendEmail() {
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setData(Uri.parse("mailto:"));
		String[] to = { "cuong.9321@gmail.com", "cuong.9312@yahoo.com" };
		String[] cc = null;
		emailIntent.putExtra(Intent.EXTRA_EMAIL, to);
		emailIntent.putExtra(Intent.EXTRA_CC, cc);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT,
				"Góp ý về phần mềm đọc truyện");
		emailIntent.putExtra(Intent.EXTRA_TEXT, "Viết lời góp ý tại đây");
		emailIntent.setType("message/rfc822");
		startActivity(Intent.createChooser(emailIntent, "Email"));
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(
					MainActivity.this);
			// alertDialog.setTitle("Confirm Exit...");
			alertDialog.setMessage("Bạn chắc chắn muốn thoát?");
			alertDialog.setIcon(R.drawable.delete);
			alertDialog.setPositiveButton("YES",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							System.exit(0);
						}
					});
			alertDialog.setNegativeButton("NO",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							dialog.cancel();
						}
					});
			// alertDialog.setIcon(R.drawable.)
			alertDialog.show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}
}
