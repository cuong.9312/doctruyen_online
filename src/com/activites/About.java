package com.activites;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class About extends Activity {
	TextView tViewVersion;
	Button btnClose;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		btnClose = (Button) findViewById(R.id.btnClose);
		tViewVersion = (TextView) findViewById(R.id.tViewVersion);
	}

	public void inits() {
//		//---hides the title bar---
//		 requestWindowFeature(Window. FEATURE_NO_TITLE);
		String about="Tên ứng dung: đọc truyện chữ"+
		"\n\n"+"Chức năng: đọc truyện tranh online, tìm kiếm, lưu trữ truyện"+
		"\n\n"+"Phiên bản: "+checkVersion();
		tViewVersion.setText(about);
	}

	public void addEvents() {
		btnClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	private String checkVersion() {
		PackageManager pm = getPackageManager();
		try {
			// ---get the package info---
			PackageInfo pi = pm.getPackageInfo(
					"com.activites", 0);
			// ---return the versioncode---
			return Integer.toString(pi.versionCode);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.about, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
