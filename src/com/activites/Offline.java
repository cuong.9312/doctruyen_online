package com.activites;

import java.io.File;
import java.util.ArrayList;

import Model.Book;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Offline extends Activity {
	static final int deleteBack = 1;
	ListView listView;
	static ArrayList<Book> listOffline = new ArrayList<Book>();
	ArrayList<Book> arr;
	ArrayAdapter<Book> adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_truyen);
		getControls();
		inits();
		addEvents();
	}

	public void getControls() {
		listView = (ListView) findViewById(R.id.listView);
	}

	public void inits() {
		try {
			this.setTitle("Truyện đã lưu");
			setFontAndActionBar();
			arr = new ArrayList<Book>();

			// ---SD Storage---
			File sdCard = Environment.getExternalStorageDirectory();
			File directory = new File(sdCard.getAbsolutePath() + "/doctruyen");
			File[] listFile = directory.listFiles();
			for (int i = 0; i < listFile.length; i++) {
				String fileName = listFile[i].getName();
				Book book = new Book(fileName, fileName.substring(0,
						fileName.length() - 4), "", "");
				arr.add(book);
			}
			adapter = new ArrayAdapter<Book>(this,
					android.R.layout.simple_list_item_1, arr);
			listView.setAdapter(adapter);

		} catch (Exception ex) {
			Toast.makeText(getBaseContext(),
					"lỗi init Offline" + ex.toString(), Toast.LENGTH_SHORT)
					.show();
		}
	}

	public void setFontAndActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		 actionBar.setIcon(R.drawable.m2);
		Typeface custom_font = Typeface.createFromAsset(getAssets(),
				"fonts/Tabitha.ttf");
		int titleId = getResources().getIdentifier("action_bar_title", "id",
				"android");
		TextView yourTextView = (TextView) findViewById(titleId);
		yourTextView.setTypeface(custom_font);
	}

	public void addEvents() {
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Book book = arr.get(position);
				Intent intent = new Intent(getBaseContext(), PageReadOffline.class);
				intent.putExtra("info", book);
				startActivityForResult(intent, 1);
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		return MenuChoice(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == deleteBack) {
			finish();
			startActivity(getIntent());
		}
	}

	private boolean MenuChoice(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		case 0:
			Toast.makeText(this, "You clicked Setting item", Toast.LENGTH_SHORT)
					.show();
			return true;
		}
		return false;
	}
}
