package Model;

import java.io.Serializable;

public class Item implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String id;
	String category;
	String url;

	public Item() {
		// TODO Auto-generated constructor stub
	}

	public Item(String id, String category, String url) {
		super();
		this.id = id;
		this.category = category;
		this.url = url;
	}

	public Item(String id, String category) {
		super();
		this.id = id;
		this.category = category;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	public String toString() {
		return this.category;
	}

}
