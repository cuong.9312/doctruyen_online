package MyAdappter;

import java.util.ArrayList;

import Model.Book;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.activites.R;

public class MyArrayAdapter2 extends ArrayAdapter<Book> {
	Activity context = null;
	ArrayList<Book> arr = null;
	int layoutId;

	public MyArrayAdapter2(Activity context, int layoutId, ArrayList<Book> arr) {
		super(context, layoutId, arr);
		this.context = context;
		this.layoutId = layoutId;
		this.arr = arr;

		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		convertView = inflater.inflate(layoutId, null);
		if (arr.size() > 0 && position >= 0) {
			final TextView txtdisplay = (TextView) convertView
					.findViewById(R.id.txtitem2);
			final String bookName = arr.get(position).getTitle();
			txtdisplay.setText(bookName);
			Typeface custom_font = Typeface.createFromAsset(txtdisplay
					.getContext().getAssets(), "fonts/potter.ttf");
			txtdisplay.setTypeface(custom_font);

		}
		return convertView;
	}
}
