package MyAdappter;

import java.util.ArrayList;

import Model.Item;
import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.activites.R;

public class MyArrayAdapter extends ArrayAdapter<Item> {
	Activity context = null;
	ArrayList<Item> arr = null;
	int layoutId;

	public MyArrayAdapter(Activity context, int layoutId, ArrayList<Item> arr) {
		super(context, layoutId, arr);
		this.context = context;
		this.layoutId = layoutId;
		this.arr = arr;

		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		convertView = inflater.inflate(layoutId, null);
		if (arr.size() > 0 && position >= 0) {
			final Item item = arr.get(position);
			final TextView txtdisplay = (TextView) convertView
					.findViewById(R.id.txtitem);
			final String bookName = arr.get(position).toString();
			txtdisplay.setText(bookName);
			Typeface custom_font = Typeface.createFromAsset(txtdisplay
					.getContext().getAssets(), "fonts/Tabitha.ttf");
			txtdisplay.setTypeface(custom_font);
			final ImageView imgitem = (ImageView) convertView
					.findViewById(R.id.imgitem);
			if (item.getId().equalsIgnoreCase("coTich"))
				imgitem.setImageResource(R.drawable.cotich);
			if (item.getId().equalsIgnoreCase("truyenThuyet"))
				imgitem.setImageResource(R.drawable.truyenthuyet);
			if (item.getId().equalsIgnoreCase("andersen"))
				imgitem.setImageResource(R.drawable.andersen);
			if (item.getId().equalsIgnoreCase("grimm"))
				imgitem.setImageResource(R.drawable.grim);
			if (item.getId().equalsIgnoreCase("theGioi"))
				imgitem.setImageResource(R.drawable.thegioi);
			if (item.getId().equalsIgnoreCase("thanThoai"))
				imgitem.setImageResource(R.drawable.thanthoai);
			if (item.getId().equalsIgnoreCase("khac"))
				imgitem.setImageResource(R.drawable.khac);
			if (item.getId().equalsIgnoreCase("daLuu"))
				imgitem.setImageResource(R.drawable.daluu);
		}
		return convertView;
	}
}
